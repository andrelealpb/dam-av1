package com.aliandro.bugtracker.model;

import javax.ejb.Local;

import com.aliandro.bugtracker.model.Issue;

@Local
public interface BugTracker {

	public void addIssue( Issue issue );
	
}
